package v1.gitlab

import javax.inject.Inject

import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import play.api.libs.json._

import scala.concurrent.{ ExecutionContext, Future }

case class PostFormInput(title: String, body: String)

case class KeyValue(key: String, value: String)

case class ObjectAttributes(
  id:          Int,
  ref:         String,
  tag:         Boolean,
  sha:         String,
  before_sha:  String,
  source:      String,
  status:      String,
  stages:      List[String],
  created_at:  String,
  finished_at: Option[String],
  duration:    Option[Int],
  variables:   List[KeyValue])

case class User(name: String, username: String, avatar_url: String)

case class Project(
  id:                  Int,
  name:                String,
  description:         String,
  web_url:             String,
  avatar_url:          Option[String],
  git_ssh_url:         String,
  git_http_url:        String,
  namespace:           String,
  visibility_level:    Int,
  path_with_namespace: String,
  default_branch:      String)

case class Author(name: String, email: String)
case class Commit(
  id:        String,
  message:   String,
  timestamp: String,
  url:       String,
  author:    Author)

case class WebhookPost(
  object_kind:       String,
  object_attributes: Option[ObjectAttributes],
  user:              Option[User],
  project:           Project,
  commit:            Commit)

/**
 * Takes HTTP requests and produces JSON.
 */
class WebhookController @Inject() (cc: PostControllerComponents)(
  implicit
  ec: ExecutionContext)
  extends PostBaseController(cc) {

  private val logger = Logger(getClass)

  implicit val authorReads = Json.reads[Author]
  implicit val commitReads = Json.reads[Commit]
  implicit val projectReads = Json.reads[Project]
  implicit val userReads = Json.reads[User]
  implicit val keyValueReads = Json.reads[KeyValue]
  implicit val objectAttributeReads = Json.reads[ObjectAttributes]
  implicit val webhookPostReads = Json.reads[WebhookPost]

  def index: Action[AnyContent] = PostAction.async { implicit request =>
    postResourceHandler.find.map { posts =>
      Ok(Json.toJson(posts))
    }
  }

  def process: Action[AnyContent] = PostAction.async { implicit request =>
    Future {
      val json: JsValue = request.body.asJson.get
      logger.debug("got json: " + json.toString)
      val input = json.as[WebhookPost]
      postResourceHandler.create(input).map { post =>
        logger.info(post.toString)
      }
      logger.info("responding OK")
      Created("OK")
    }
  }

  def show(id: String): Action[AnyContent] = PostAction.async {
    implicit request =>
      logger.debug(s"show: id = $id")
      postResourceHandler.lookup(id).map { post =>
        Ok(Json.toJson(post))
      }
  }

}
