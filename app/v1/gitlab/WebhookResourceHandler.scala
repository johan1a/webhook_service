package v1.gitlab

import javax.inject.{Inject, Provider}
import play.api.{Configuration, Logger, MarkerContext}

import scala.concurrent.{Await, ExecutionContext, Future}
import play.api.libs.json._
import play.api.libs.ws.{WSClient, WSResponse}

import scala.concurrent.duration.Duration
import scala.util.{Failure, Success, Try}

/**
 * DTO for displaying post information.
 */
case class PostResource(id: String, link: String, title: String, body: String)

object PostResource {
  /**
   * Mapping to read/write a PostResource out as a JSON value.
   */
  implicit val format: Format[PostResource] = Json.format
}

case class DeploymentInfo(imageName: String, deploymentName: String)

case class Deployment(deploymentType: String,
                      imageReference: String,
                      deploymentName: String,
                      containerName: String)


/**
 * Controls access to the backend data, returning [[PostResource]]
 */
class WebhookResourceHandler @Inject()(config: Configuration,
                                       ws: WSClient,
                                       routerProvider: Provider[WebhookRouter],
                                       postRepository: PostRepository)(implicit ec: ExecutionContext) {

  private val logger = Logger(this.getClass)

  implicit val deploymentReads: Reads[Deployment] = Json.reads[Deployment]
  implicit val deploymentWrites: Writes[Deployment] = Json.writes[Deployment]

  implicit val deploymentInfoReads: Reads[DeploymentInfo] = Json.reads[DeploymentInfo]

  private val deploymentsUrl: String = s"${config.get[String]("deployer.url")}/api/deployments"

  def create(postInput: WebhookPost)(implicit mc: MarkerContext): Future[WebhookPost] = {
    logger.info("creating deployment")
    val status = postInput.object_attributes.map(_.status).getOrElse("failed")
    if (postInput.object_kind == "pipeline" && status == "success") {
      Future {
        val infoUrl = s"${postInput.project.web_url}/raw/master/deployment-info.json"
        logger.info("infoUrl: " + infoUrl)
        ws.url(infoUrl).get().map(response => {
          logger.info("got deployment-info.json: " + response)
          val deploymentInfo: DeploymentInfo = Json.fromJson[DeploymentInfo](response.json).get
          val image: String = s"${deploymentInfo.imageName}:${postInput.commit.id}"
          val deployment = Deployment(imageReference = image, deploymentName = deploymentInfo.deploymentName, containerName = "", deploymentType = "")
          val request = ws.url(deploymentsUrl)
          request.post(Json.toJson(deployment)).onComplete {
            case Success(x) =>
              logger.info(s"\nresult = $x")
            case Failure(e) =>
              logger.error(s"\nFailure: result = $e")
          }
        }).onComplete {
          case Success(x) =>
            logger.info(s"\nresult = $x")
          case Failure(e) =>
            logger.error(s"\nFailure: result = $e")
        }
        postInput
      }
    } else {
      logger.debug(s"object_kind: ${postInput.object_kind}, status: ${status}, Ignoring.")
      Future {
        postInput
      }
    }
  }

  def lookup(id: String)(
    implicit
    mc: MarkerContext): Future[Option[PostResource]] = {
    val postFuture = postRepository.get(PostId(id))
    postFuture.map {
      maybePostData =>
        maybePostData.map {
          postData =>
            createPostResource(postData)
        }
    }
  }

  def find(implicit mc: MarkerContext): Future[Iterable[PostResource]] = {
    postRepository.list().map {
      postDataList =>
        postDataList.map(postData => createPostResource(postData))
    }
  }

  private def createPostResource(p: PostData): PostResource = {
    PostResource(p.id.toString, routerProvider.get.link(p.id), p.title, p.body)
  }

}
